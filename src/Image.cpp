#include <iostream>
#include <cassert>
#include <fstream>
#include <string>
#include "Pixel.h"
#include "Image.h"

using namespace std;
// Constructeur par défaut de la classe Image
Image::Image() :  dimx(0),  dimy(0),  tab(nullptr) {}

// Constructeur surchargé de la classe Image
Image::Image(int dimensionX, int dimensionY) {
    assert( dimensionX > 0 &&  dimensionY > 0);
    this->dimx= dimensionX;
    this->dimy= dimensionY;
    tab = new Pixel[dimx *  dimy];
}

// Destructeur de la classe Image
Image::~Image()
{
    dimx = 0;
    dimy = 0;
    //test pas necessaire
    if (tab!= nullptr) {
        delete [] tab;
    }
    tab = nullptr;
}


bool Image::validCoordonnee(int x, int y) const
{
    return ((x>=0 && x<dimx) && (y>=0 && y<dimy));
}

// Méthode pour obtenir un Pixel à partir des coordonnées (x, y) - version non-constante
Pixel& Image::getPix(int x, int y) {
    assert(x >= 0 && x <  dimx && y >= 0 && y <  dimy);
    return  tab[y *  dimx + x];
}

// Méthode pour obtenir un Pixel à partir des coordonnées (x, y) - version constante
Pixel Image::getPix(int x, int y) const {
    assert(x >= 0 && x <  dimx && y >= 0 && y <  dimy);
    return  tab[y *  dimx + x];
}

//getter sur x
int Image::getDimx() const {return this->dimx;}
//getter sur y
int Image::getDimy() const {return this->dimy;}


// Méthode pour définir un Pixel à partir des coordonnées (x, y)
void Image::setPix(int x, int y, const Pixel& couleur) {
    assert(x >= 0 && x <  dimx && y >= 0 && y <  dimy);
     tab[y *  dimx + x] = couleur;
}

// Méthode pour dessiner un rectangle de couleur donnée aux coordonnées spécifiées
void Image::dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, const Pixel & couleur)
{
    assert(validCoordonnee(Xmin,Ymin) && validCoordonnee(Xmax,Ymax));
        for(int y=Ymin;y<=Ymax;y++)
            for(int x=Xmin;x<=Xmax;x++)
                setPix(x,y,couleur);
}

// Méthode pour effacer l'image en la remplissant avec une couleur donnée
void Image::effacer(const Pixel& pixel) {
    dessinerRectangle(0, 0,  dimx - 1,  dimy - 1, pixel);
}

//new test

void Image::testRegression()
{
    Image im1(10,10);
    // test de la validié du pixel (il est dans le tableau)
    assert(im1.validCoordonnee(2,3) == true);

    // test de getPix et setPix (couleur qui entre = couleur qui sort)
    Pixel pixel (65,0,155);
    im1.setPix(9,3,pixel);
    assert(im1.getPix(9,3) == pixel);

    // test sur le rectangle
    Pixel pixel0(10,20,20);
    im1.dessinerRectangle(0,0,im1.dimx-1,im1.dimy-1,pixel0);

    for(int y=0;y<im1.dimy;y++)
    {
        for(int x=0;x<im1.dimx;x++)
        {
            assert((im1.getPix(x,y) == pixel0));
        }
    }

    // test sur effacer avec un nouveau rectagle
    Pixel other(200,25,20);
    im1.effacer(other);

    for(int y=0;y<im1.dimy;y++)
    {
        for (int x = 0; x < im1.dimx; x++)
        {
            assert((im1.getPix(x, y) == other));
        }
    }

    Pixel& copie = im1.getPix(2,5);
    {
        copie.b=125;
        copie.g = 210;
    }


    assert(im1.getPix(2,5).b == 125);
    assert(im1.getPix(2,5).g == 210);

    im1.sauver("./data/imageTest.ppm");
    std::cout<< "test de regression Complet\n";
}



// Méthode pour sauvegarder l'image dans un fichier
void Image::sauver(const string &filename) const {
    ofstream fichier(filename.c_str());
    assert(fichier.is_open()); // Vérification si le fichier est correctement ouvert

    fichier << "P3" << endl;
    fichier <<  dimx << " " <<  dimy << endl;
    fichier << "255" << endl;

    for ( int y = 0; y <  dimy; ++y) {
        for (  int x = 0; x <  dimx; ++x) {
            Pixel pix = getPix(x, y);
            fichier << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
    }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

// Méthode pour ouvrir une image à partir d'un fichier
void Image::ouvrir(const string &filename) {
    ifstream fichier(filename.c_str());
    assert(fichier.is_open()); // Vérification si le fichier est correctement ouvert

    char r, g, b;
    string mot;
     dimx =  dimy = 0;

    fichier >> mot >>  dimx >>  dimy >> mot; // Lecture des dimensions de l'image

    assert( dimx > 0 &&  dimy > 0);

    if ( tab != nullptr)
        delete[]  tab;

     tab = new Pixel[ dimx *  dimy];

    for (  int y = 0; y <  dimy; ++y) {
        for (  int x = 0; x <  dimx; ++x) {
            fichier >> r >> g >> b; // Lecture des composantes RGB
            getPix(x, y).r = r;
            getPix(x, y).g = g;
            getPix(x, y).b = b;
        }
    }

    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

// Méthode pour afficher les valeurs des pixels sur la console
void Image::afficherConsole() {
    cout <<  dimx << " " <<  dimy << endl;
    for (  int y = 0; y <  dimy; ++y) {
        for (  int x = 0; x <  dimx; ++x) {
            Pixel pix = getPix(x, y);
            cout << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        cout << endl;
    }
}





