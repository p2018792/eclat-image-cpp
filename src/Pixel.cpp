#include "Pixel.h"
#include <iostream>

//surchage de l'operator ==
bool Pixel::operator==(const Pixel& pixel) const {
    return r == pixel.r && g == pixel.g && b == pixel.b;
}

//affichage d'un pixel en utilisant la Surcharge de l'opérateur de sortie pour afficher un pixel.
void Pixel::afficherPixel(const Pixel& pixel)
{
    std::cout << pixel;
}